package com.errigal.client;

import grails.plugins.gwt.shared.Response;

public class ServerTimeResponse implements Response {
    private static final long serialVersionUID = 1L;

    private String result;

    private ServerTimeResponse() {}

    public ServerTimeResponse(String time) {

        this.result = time;
    }

    public String getTime(){

        return this.result;
    }
}
