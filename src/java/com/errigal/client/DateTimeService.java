package com.errigal.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

public interface DateTimeService extends RemoteService {

    java.lang.String getDateTime();
}
