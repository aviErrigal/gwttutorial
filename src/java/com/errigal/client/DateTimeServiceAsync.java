package com.errigal.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DateTimeServiceAsync {

    void getDateTime(AsyncCallback A);
}
