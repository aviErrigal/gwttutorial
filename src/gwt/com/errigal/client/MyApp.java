package com.errigal.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.*;


import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import grails.plugins.gwt.client.GwtActionService;
import grails.plugins.gwt.client.GwtActionServiceAsync;


@RemoteServiceRelativePath("rpc")

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MyApp implements EntryPoint {
    /**
     * This is the entry point method.
     */

    DateTimeServiceAsync myService;
    VerticalPanel mainPanel = new VerticalPanel();
    HorizontalPanel subPanel = new HorizontalPanel();
    final Button submitButton = new Button("Get RPC DateTime");
    final Button submitButton02 = new Button("Get ActionHandler DateTime");
    final Label label01 = new Label("Time");

    public void onModuleLoad() {


        myService = (DateTimeServiceAsync) GWT.create(DateTimeService.class);
        ServiceDefTarget endpoint = (ServiceDefTarget) myService;
        // Note the URL where the RPC service is located!
        String moduleRelativeURL = GWT.getModuleBaseURL() + "rpc";
        endpoint.setServiceEntryPoint(moduleRelativeURL);




        getDateTimeRPC();



        submitButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                getDateTimeRPC();


            }
        });

        submitButton02.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                getActionHandlerDateTime();


            }
        });


        subPanel.add(submitButton);
        subPanel.add(submitButton02);
        subPanel.add(label01);
        mainPanel.add(subPanel);

        RootPanel panel = RootPanel.get("mainDiv");
        panel.add(mainPanel);

    }


    private void getDateTimeRPC(){


        myService.getDateTime(new AsyncCallback() {

            public void onFailure(Throwable arg0) {

                System.out.println(arg0.getMessage());

            }

            public void onSuccess(Object time) {

                label01.setText(time.toString());

            }

        });

    }

    protected void getActionHandlerDateTime() {
        GwtActionServiceAsync service = GWT.create(GwtActionService.class);
        ((ServiceDefTarget) service).setServiceEntryPoint(GWT.getModuleBaseURL() + "rpc");

        service.execute(new ServerTimeAction(), new AsyncCallback<ServerTimeResponse>() {
            public void onFailure (Throwable caught) {

                System.out.println(caught.getMessage());


            }

            public void onSuccess (ServerTimeResponse result) {

                label01.setText(result.getTime());

            }
        });
    }

}
