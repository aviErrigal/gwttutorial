package com.errigal

import com.errigal.client.ServerTimeAction
import com.errigal.client.ServerTimeResponse
import gwttutorial.DateTimeService

class ServerTimeActionHandler {
    ServerTimeResponse execute(ServerTimeAction action) {

        DateTimeService dts = new DateTimeService()
        return new ServerTimeResponse(dts.getDateTime())
    }
}
