package gwttutorial

import grails.transaction.Transactional

@Transactional
class DateTimeService {

  boolean transactional = true
  static expose = ["gwt:com.errigal.client"]


    String getDateTime(){

        String returnVal = new Date().getDateTimeString();
        return returnVal
    }
}
